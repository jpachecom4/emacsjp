(setq org-capture-templates
        '(("t" "TODO" entry (file "~/.emacs.d/org/agenda.org")
        "* TODO %? \n  %U" :empty-lines 0)
        ("s" "Scheduled TODO" entry (file "~/.emacs.d/org/agenda.org")
        "* TODO %?  \nSCHEDULED: %^t\n  %U" :empty-lines 1)
        ("d" "Deadline" entry (file "~/.emacs.d/org/agenda.org")
            "* TODO %? \n  DEADLINE: %^t" :empty-lines 1)
        ("p" "Priority" entry (file+datetree "~/.emacs.d/org/agenda.org")
        "* TODO [#A] %? \n  SCHEDULED: %^t")
        ("a" "Appointment" entry (file "~/.emacs.d/org/agenda.org")
         "* %? \n  %^t")
        ("n" "Note" entry (file+datetree "~/.emacs.d/org/agenda.org")
            "* %? \n%U" :empty-lines 0)
        ("T" "amm TODO" entry (file "~/.emacs.d/org/amm.org")
        "* TODO %? \n  %U" :empty-lines 0)
        ("S" "amm Scheduled TODO" entry (file "~/.emacs.d/org/amm.org")
        "* TODO %?  \nSCHEDULED: %^t\n  %U" :empty-lines 1)
        ("N" "amm Note" entry (file+datetree "~/.emacs.d/org/notes.org")
            "* %? \n%U" :empty-lines 0)
        ("F" "Reporte de fallas" entry
	 (file+headline "reportedefallas.org" "Reporte de fallas Segundo Turno.")
	 "* %^{Equipo donde se trabajo|Celda 1|Celda 2|Celda 3|Celda 4|Celda 5|Celda 6|Celda 7|Celda 9|Celda 11|Celda 12|Celda 13|Celda 14}
Buhler: %?\n
Quench: \n
Kuka/REIS: \n
LaserMarker: \n
Maquinado: \n
MDO/MVE: \n
MPS144: \n")))
