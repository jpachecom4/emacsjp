
(setq
user-full-name "Javier Pacheco"
      user-mail-address "jpachecom@gmail.com")
(setq doom-theme 'doom-nord
display-line-numbers-type 'relative)
(setq org-directory "~/org/")

(use-package org-auto-tangle
  :defer t
  :hook(org-mode . org-auto-tangle-mode)
  :config
  (setq org-auto-tangle-default t))
