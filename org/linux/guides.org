#+title: Linux Guide to dont F**k up the system.
#+author: Javier Pacheco.
#+INCLUDE: "~/.emacs.d/org/orgconfig.org"


* Instalacion del sistema operativo.

Una serie de pasos y /tips/ de instalacion de ciertos sistemas que e instalado a lo largo de mi estancia en linux.
** Void Linux *musl*
Este esta sencillo dado que tiene su propio instalador y es facil sobrellevarlo.

*** Instalador "GUI"
#+begin_example
# void-installer
#+end_example

** Artix Linux:
*** Hacer las particiones:
#+begin_example
cfdisk 
#+end_example

*** Dar formato a las particiones:
#+begin_example
mkfs.ext4 (root, home) mkfs.fat -F32 /dev/sda(x)
#+end_example

*** Dar formato a las particiones y montarlas.
Lo hago en una sola linea para "ahorrar tiempo"
#+begin_example
mount /dev/sda(x) /mnt ; mkdir /mnt/boot ; mount /dev/sda(x) /mnt/boot
#+end_example

*** Hacer el basestrap
Aquise instalan los paquetes primordiales como el *kernel*, la *base*, etc.etc. y vim para editar algunas cosas.
#+begin_example
basestrap /mnt base base-devel runit elogind-runit linux linux-firmware vim
#+end_example

*** Generar el FSTAB.
#+begin_example
fstabgen -U /mnt >> /mnt/etc/fstab
#+end_example

*** Chroot al sistema.
#+begin_example
artix-chroot /mnt
#+end_example

*** Editar los mirrorlist.
#+begin_example
vim /etc/pacman.d/mirrorlist
#+end_example

*** Seteo de la zona horaria.
#+begin_example
ln -sf /usr/share/zoneinfo/America/Matamoros /etc/localtime
#+end_example

*** Seteo del reloj.
#+begin_example
hwclock --systohc
#+end_example

*** Seteo del lenguaje
#+begin_example
vim /etc/locale.gen
en_US (both UTF-8 and ISO) 
locale-gen 
vim /etc/locale.conf 
LANG=en_US.UTF-8 
#+end_example

*** Instalacion de alguns paquetes importantes.
#+begin_example
pacman -S networkmanager networkmanager-runit grub os-prober efibootmgr
ln -s /etc/runit/sv/NetworkManager /etc/runit/runsvdir/default/ 
#+end_example

*** Edicion del hostname y varias variables.

#+begin_example
vim /etc/hostname
#+end_example
Aqui configuras como quieres que aparesca el nombre de tu ordenador:
javier@artixlinuxlaptop

#+begin_example
vim /etc/hosts

127.0.0.1 	localhost 
::1 		localhost 
127.0.0.1 	artix.localdomain artix 
#+end_example

*** Instalacion de grub
#+begin_example
grub-install –target=x86_64-efi –efi-directory=/boot –bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg 
#+end_example

*** Seteo del passwrd de *ROOT*
#+begin_example
passwd
#+end_example

*** Posibles errores si falla el instalador.
#+begin_example
#First updates the mirrors: 
Pacman -Syu 
Vim /etc/pacman.conf #(add this:) 
[extra] 
Include = /etc/pacman.d/mirrorlist-arch 
[community] 
Include = /etc/pacman.d/mirrorlist-arch 
[multilib] 
Include = /etc/pacman.d/mirrorlist-arch 
Vim /etc/pacman.d/mirrorlist-arch 
#comment the worldwide mirrors, and uncomment the USA mirrors.

#+end_example

Installing LARBS (because I like it): 
1. curl -LO larbs.xyz/larbs.sh 
2. sh larbs.sh 
3. wait until the script finish. 
4. Enjoy


** Arch Linux

*** Hacer las particiones:
#+begin_example
cfdisk 
#+end_example

*** Dar formato a las particiones:
#+begin_example
mkfs.ext4 (root, home) mkfs.fat -F32 /dev/sda(x)
#+end_example

*** Dar formato a las particiones y montarlas.
Lo hago en una sola linea para "ahorrar tiempo"
#+begin_example
mount /dev/sda(x) /mnt ; mkdir /mnt/boot ; mount /dev/sda(x) /mnt/boot
#+end_example

*** Hacer el basestrap
Aquise instalan los paquetes primordiales como el *kernel*, la *base*, etc.etc. y vim para editar algunas cosas.
#+begin_example
pacstrap /mnt base base-devel linux linux-firmware vim
#+end_example

*** Generar el FSTAB.
#+begin_example
genfstab -U /mnt >> /mnt/etc/fstab
#+end_example

*** Chroot al sistema.
#+begin_example
arch-chroot /mnt
#+end_example

*** Editar los mirrorlist.
#+begin_example
vim /etc/pacman.d/mirrorlist
#+end_example

*** Seteo de la zona horaria.
ln -sf /usr/share/zoneinfo/[Region]/[City] /etc/localtime

#+begin_example
ln -sf /usr/share/zoneinfo/America/Matamoros /etc/localtime
#+end_example

*** Seteo del reloj.
#+begin_example
hwclock --systohc
#+end_example

*** Seteo del lenguaje
#+begin_example
vim /etc/locale.gen
en_US (both UTF-8 and ISO) 
locale-gen 
vim /etc/locale.conf 
LANG=en_US.UTF-8 
#+end_example

*** Instalacion de alguns paquetes importantes.
#+begin_example
pacman -S networkmanager grub os-prober efibootmgr
systemctl enable NetworkManager
#+end_example

*** Edicion del hostname y varias variables.
vim /etc/hostname

#+begin_example
vim /etc/hostname
#+end_example
Aqui configuras como quieres que aparesca el nombre de tu ordenador:
javier@archlinuxlaptop

#+begin_example
vim /etc/hosts 
127.0.0.1 	localhost 
::1 		localhost 
127.0.0.1 	arch.localdomain arch 
#+end_example

*** Instalacion de grub
#+begin_example
grub-install –target=x86_64-efi –efi-directory=/boot –bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg 
#+end_example

*** Seteo del passwrd de *ROOT*
#+begin_example
passwd
#+end_example


* GNU-Stow.
Manejador de dotfiles.
Este software te permite hacer links simbolicos para manjar tus dotfiles desde un directorio remoto y unico. Asi no hay que buscar archivos por aqui y por aya.

** Instalacion.
Dependiendo del OS que se haya elegido para instalar stow se tendra que insertar el comando de instalacion del manejador de paquetes de dicho OS:

*** VOID
#+begin_example
xbps-install stow
#+end_example

*** Artix/Arch
#+begin_example
sudo pacman -S stow
#+end_example

** Creacion de los symlinks y uso de stow
basicamente stow toma de base el uso de los directorios tal como se maneja en Gnu/linux:
Algo asi como */home/user/.config*
tomando como raiz el directorio home y as va siguiendo la estructura. En este ejemplo tenemos mis dotfiles como ejemplo.

Mis archivos estan en una carpeta llamada .dotfiles, la cual contiene la siguiente estructura:
 [[./tree.png][click para abirir imagen]]
 Entonces stow lo que hara es crear links symbolicos a partir de la carpeta raiz que dentro de la carpeta .dotfiles se llama *home* (puede ser cualquier otro nombre pero las llame asi para que se sobreentienda como va a ser el cpiado de links)

 Y el comando para crear los links es el siguiente:

#+begin_example
cd .dotfiles
stow -d home
#+end_example

Y listo, si quisieras borrar o eliminar los links symoblicos usa el siguiente comando:

#+begin_example
stow -D home
#+end_example

